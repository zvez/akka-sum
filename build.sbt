name := "akka-sum"
version := "1.0"
scalaVersion := "2.11.8"

enablePlugins(JavaAppPackaging)

maintainer := "zvez"

mainClass in Compile := Some("probe.RemoteRunner")

parallelExecution in Test := false

val akkaVersion = "2.4.7"

libraryDependencies ++= Seq(

  "org.scalatest" %% "scalatest" % "2.2.6" % "test",

  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-remote" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",

  "com.github.scopt" %% "scopt" % "3.4.0",

  "ch.qos.logback" %  "logback-classic" % "1.1.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0"
)
