#!/usr/bin/env bash
for (( i=0; i < $2; i++ ))
do
    echo "Running container for node $i"
    docker run --net=akka -itd --name=node-${i} akka-sum:1.0 --resolve-strategy host --hostname=node-${i} --algorithm $1 --current-node ${i} --nodes-count $2
done

echo "Waiting nodes to finish..."

for (( i=0; i < $2; i++ ))
do
    docker wait node-${i} > /dev/null
    docker logs node-${i} | grep "secret value:"
done

echo "Results:"

for (( i=0; i < $2; i++ ))
do
    docker logs node-${i} | grep "finished with result"
    docker rm node-${i} > /dev/null
done

echo "Done"


