package probe

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.{ActorSystem, Props}
import com.typesafe.scalalogging.LazyLogging
import probe.Actors.{Context, LocalNodeResolver, MasterActor, Reporter}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

/**
  * Author: Andrey Zinovyev
  * Date: 05.06.2016
  */
object LocalRunner extends LazyLogging {


  case class NodeOpsStat(messages: AtomicInteger = new AtomicInteger(0), sums: AtomicInteger = new AtomicInteger(0))

  class LocalReporterWithShutdown(nodesCount: Int, actorSystem: ActorSystem) extends Reporter {
    val reported = new AtomicInteger(0)
    val opsStat = (0 until nodesCount).map(idx => idx -> NodeOpsStat()).toMap

    def reportResult(idx: Int, result: Int) = {
      logger.info(s"Node $idx finished with result: $result")
      if (reported.incrementAndGet() == nodesCount) {
        logger.info("All nodes reported. Shutting down actor system")
        actorSystem.terminate()
      }
    }

    override def messageSent(idx: Int) =
      opsStat(idx).messages.incrementAndGet()

    override def sumPerformed(idx: Int) = opsStat(idx).sums.incrementAndGet()
  }

  def main(args: Array[String]): Unit = {
    args match {
      case Array(algorithm, nodesCountS) =>
        run(algorithm, nodesCountS.toInt)
      case _ =>
        println("Usage: LocalRunner algorithmName nodesCount")
        println(s"Available algorithms: ${Actors.algorithms.keys.mkString(", ")}")
    }

  }

  def run(algorithm: String, nodes: Int): Unit = {
    logger.info(s"Going to run $algorithm on $nodes nodes")

    val system = ActorSystem("akka-sum")


    val ctx = new Context {
      val nodesCount = nodes
      val resolver = new LocalNodeResolver(system)
      val reporter = new LocalReporterWithShutdown(nodes, system)
    }

    system.actorOf(Props.apply(new MasterActor(nodes, ctx)), Actors.masterName)

    for (idx <- 0 until nodes)
      system.actorOf(Actors.actorForAlg(algorithm, idx, ctx), Actors.nodeName(idx))

    Await.ready(system.whenTerminated, nodes seconds)

    val stat = ctx.reporter.opsStat.toSeq.sortBy(_._1)
    val toDisplay = stat.map {case (idx, NodeOpsStat(msgs, sums)) => s"$idx\t|$msgs\t\t|$sums"}
    logger.info(
      s"""
         |Some statistics:
         |Node|Messages|Sum ops
         |${toDisplay.mkString("\n")}
         |*   |${stat.map(_._2.messages.intValue()).sum}      |${stat.map(_._2.sums.intValue()).sum}
          """.stripMargin)
    logger.info("Done")

    system.terminate()
  }

}
