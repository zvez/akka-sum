package probe

import akka.actor.{Actor, ActorPath, ActorRef, ActorSelection, ActorSystem, Props}

import scala.util.Random


/**
  * Author: Andrey Zinovyev
  * Date: 05.06.2016
  */
object Actors {

  case class Start()
  case class RegisterMe(idx: Int)

  //we can move whole nodes resolution here: simply send all nodes refs to everyone. Not very good for large nodes count though
  class MasterActor(nodesCount: Int, ctx: Context) extends Actor with akka.actor.ActorLogging {

//    val registered = Array.ofDim[ActorRef](nodesCount)
    var registeredCount = 0

    override def preStart() = {
      log.info(s"Starting master actor")
    }

    def receive = {
      case RegisterMe(nodeIdx) =>
//        registered(nodeIdx) = sender()
        registeredCount += 1
        if (registeredCount == nodesCount) {
          log.info("All nodes registered. Lets start")
          (0 until nodesCount).foreach(idx => ctx.resolver.resolveNode(idx) ! Start())
        }
    }
  }

  abstract class SumActor(nodeIdx: Int, ctx: Context, secretValue: Int) extends Actor with akka.actor.ActorLogging {

    override def preStart() = {
      log.info(s"Starting node $nodeIdx at path ${self.path}")
      log.info(s"Node $nodeIdx secret value: $secretValue")
      ctx.resolver.master ! RegisterMe(nodeIdx)
    }

    def receive = {
      case Start() =>
        log.debug(s"Node $nodeIdx started")
        postStart()
    }

    def sendToNode(idx: Int, value: Int): Unit = {
      log.debug(s"Sending value $value to $idx")
      ctx.reporter.messageSent(nodeIdx)
      ctx.resolver.resolveNode(idx) ! value
    }

    def sum(a: Int, b: Int) = {
      ctx.reporter.sumPerformed(nodeIdx)
      a + b
    }

    def postStart(): Unit
  }

  //todo race condition if node start getting Int messages before getting Start
  class HiveActor(nodeIdx: Int, ctx: Context, secretValue: Int) extends SumActor(nodeIdx, ctx, secretValue) {
    var acc = secretValue
    var received = 1

    override def receive = super.receive.orElse {
      case v: Int if received < ctx.nodesCount =>
        log.debug(s"Received value from ${sender().path}")
        acc = sum(acc, v)
        received += 1
        if (received == ctx.nodesCount)
          ctx.reporter.reportResult(nodeIdx, acc)
    }

    def postStart(): Unit = {
      for (idx <- 0 until ctx.nodesCount if idx != nodeIdx) {
        sendToNode(idx, secretValue)
      }
    }
  }

  class MasterMindActor(nodeIdx: Int, ctx: Context, secretValue: Int) extends SumActor(nodeIdx, ctx, secretValue) {
    var acc = secretValue
    var received = 1

    override def receive = super.receive.orElse {
      case v: Int if nodeIdx == 0 && received < ctx.nodesCount =>
        log.debug(s"Received value from ${sender().path}")
        acc = sum(acc, v)
        received += 1
        if (received == ctx.nodesCount) {
          for (idx <- 1 until ctx.nodesCount)
            sendToNode(idx, acc)
          ctx.reporter.reportResult(nodeIdx, acc)
        }

      case v: Int if nodeIdx != 0 =>
        ctx.reporter.reportResult(nodeIdx, v)
    }

    def postStart() = {
      if (nodeIdx != 0) sendToNode(0, secretValue)
    }
  }

  class CircleActor(nodeIdx: Int, ctx: Context, secretValue: Int) extends SumActor(nodeIdx, ctx, secretValue) {
    override def receive = super.receive.orElse {
      case v: Int => sumAndPass(v)
    }

    def postStart() = if (nodeIdx == 0) {
      sumAndPass(0)
    }

    def sumAndPass(acc: Int): Unit = {
      sendToNode((nodeIdx + 1) % ctx.nodesCount, sum(acc, secretValue))
      context.become {
        case v: Int =>
          ctx.reporter.reportResult(nodeIdx, v)
          if (!isLast) sendToNode(nodeIdx + 1, v)
      }
    }

    val isLast = nodeIdx == ctx.nodesCount - 1
  }

  class TreeActor(nodeIdx: Int, ctx: Context, secretValue: Int) extends SumActor(nodeIdx, ctx, secretValue) {

    var msgReceived = 0
    var acc = secretValue

    override def receive = super.receive.orElse {
      case v: Int =>
        acc = sum(acc, v)
        msgReceived += 1
        if (msgReceived == childrenCount) {
          if (isRoot) reportAndShareResult(acc)
          else passToParentAndWait(acc)
        }
    }

    def postStart() = {
      if (isLeaf) passToParentAndWait(secretValue)
    }

    def passToParentAndWait(value: Int): Unit = {
      sendToNode(parentIdx, value)
      context.become {
        case v: Int =>
          reportAndShareResult(v)
      }
    }

    def reportAndShareResult(v: Int): Unit = {
      if (nodeExists(leftChildIdx)) {
        sendToNode(leftChildIdx, v)
        if (nodeExists(rightChildIdx)) {
          sendToNode(rightChildIdx, v)
        }
      }
      ctx.reporter.reportResult(nodeIdx, v)
    }

    val leftChildIdx = nodeIdx * 2 + 1
    val rightChildIdx = nodeIdx * 2 + 2
    val parentIdx = (nodeIdx - 1) / 2
    val isRoot = nodeIdx == 0
    val childrenCount = if (nodeExists(rightChildIdx)) 2 else if (nodeExists(leftChildIdx)) 1 else 0
    val isLeaf = childrenCount == 0
    def nodeExists(idx: Int) = idx < ctx.nodesCount
  }


  trait NodeResolver {
    def resolveNode(nodeIdx: Int): ActorSelection
    def master: ActorSelection
  }

  class LocalNodeResolver(actorSystem: ActorSystem) extends NodeResolver {
    def resolveNode(nodeIdx: Int) = {
      val path = actorSystem / Actors.nodeName(nodeIdx)
      actorSystem.actorSelection(path)
    }

    def master = actorSystem.actorSelection(actorSystem / masterName)
  }

  trait Reporter {
    def messageSent(idx: Int): Unit = {}
    def sumPerformed(idx: Int): Unit = {}
    def reportResult(idx: Int, result: Int): Unit
  }

  trait Context {
    def nodesCount: Int
    def resolver: NodeResolver
    def reporter: Reporter
  }


  val algorithms = Map (
    "hive" -> classOf[HiveActor],
    "master-mind" -> classOf[MasterMindActor],
    "circle" -> classOf[CircleActor],
    "tree" -> classOf[TreeActor]
  )

  def actorForAlg(algName: String, nodeIdx: Int, ctx: Context): Props = {
    actorForAlg(algName, nodeIdx, ctx, Random.nextInt(1000))
  }

  def actorForAlg(algName: String, nodeIdx: Int, ctx: Context, secretValue: Int): Props = {
    val clazz = algorithms.getOrElse(algName, throw new RuntimeException(s"Unknown algorithm '$algName'"))
    Props(clazz, nodeIdx, ctx, secretValue)
  }


  def nodeName(idx: Int) = s"node-$idx"
  val masterName = "master"
}
