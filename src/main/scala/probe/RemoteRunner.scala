package probe

import akka.actor.{ActorSystem, Address, Props, RootActorPath}
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import com.typesafe.scalalogging.LazyLogging
import probe.Actors.{Context, MasterActor, NodeResolver, Reporter}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

/**
  * Author: Andrey Zinovyev
  * Date: 06.06.2016
  */
object RemoteRunner extends LazyLogging {


  class SingleNodeReporter(nodeIdx: Int, actorSystem: ActorSystem) extends Reporter {
    def reportResult(idx: Int, result: Int) = {
      logger.info(s"Node $idx finished with result: $result")
      logger.info(s"Shutting down node $nodeIdx")
      actorSystem.terminate()
    }
  }

  class PortNodeResolver(hostname: String, basePortNumber: Int, actorSystem: ActorSystem) extends NodeResolver {

    def nodeAddress(nodeIdx: Int) = new Address("akka.tcp", actorSystem.name, hostname, basePortNumber + nodeIdx)

    def resolveNode(nodeIdx: Int) = {
      val path = RootActorPath(nodeAddress(nodeIdx)) / "user" / Actors.nodeName(nodeIdx)
      actorSystem.actorSelection(path)
    }

    def master = {
      val path = RootActorPath(nodeAddress(0)) / "user" / "master"
      actorSystem.actorSelection(path)
    }
  }

  class HostNodeResolver(port: Int, actorSystem: ActorSystem) extends NodeResolver {

    def nodeAddress(nodeIdx: Int) = new Address("akka.tcp", actorSystem.name, Actors.nodeName(nodeIdx), port)

    def resolveNode(nodeIdx: Int) = {
      val path = RootActorPath(nodeAddress(nodeIdx)) / "user" / Actors.nodeName(nodeIdx)
      actorSystem.actorSelection(path)
    }

    def master = {
      val path = RootActorPath(nodeAddress(0)) / "user" / "master"
      actorSystem.actorSelection(path)
    }
  }

  def main(args: Array[String]): Unit = {
    val parser = new scopt.OptionParser[Arguments]("akka-sum") {
      opt[String]('s', "resolve-strategy") action { (x, a) => a.copy(nodeResolveStrategy = x)} text "Node resolving strategy: host or port"
      opt[String]('h', "hostname") action { (x, a) => a.copy(hostname = x)} text "Host name. Default localhost"
      opt[Int]('p', "port") action { (x, a) => a.copy(port = x)} text "Bind to port. Default 2551"
      opt[String]('a', "algorithm") action { (x, a) => a.copy(algorithm = x)} text s"Algorithm to use: ${Actors.algorithms.keys.mkString(", ")}. Default 'tree'"
      opt[Int]('n', "current-node") required() action { (x, a) => a.copy(currentNode = x)} text s"Current node's index"
      opt[Int]('c', "nodes-count") required() action { (x, a) => a.copy(nodesCount = x)} text s"Total nodes count. Should be at least 2"
      help("help") text "prints this usage text"
    }

    val parsedArguments = parser.parse(args, Arguments())
    parsedArguments.foreach(run)
  }


  def run(args: Arguments): Unit = {
    logger.info(s"Starting node ${args.currentNode} with algorithm '${args.algorithm}'")
    logger.debug(s"Full args: $args")
    val config = ConfigFactory.load("application-remote.conf")
      .withValue("akka.remote.netty.tcp.hostname", ConfigValueFactory.fromAnyRef(args.hostname))
      .withValue("akka.remote.netty.tcp.port", ConfigValueFactory.fromAnyRef(args.port))
    val system = ActorSystem("akka-sum", config)

    val nodesResolver = args.nodeResolveStrategy match {
      case "port" => new PortNodeResolver(args.hostname, args.port - args.currentNode, system)
      case "host" => new HostNodeResolver(args.port, system)
      case unknown => throw new RuntimeException(s"Unknown node resolving strategy $unknown")
    }

    val ctx = new Context {
      val nodesCount = args.nodesCount
      val resolver = nodesResolver
      val reporter = new SingleNodeReporter(args.currentNode, system)
    }

    if (args.currentNode == 0) {
      logger.info("I'm the master")
      system.actorOf(Props.apply(new MasterActor(args.nodesCount, ctx)), Actors.masterName)
    }
    system.actorOf(Actors.actorForAlg(args.algorithm, args.currentNode, ctx), Actors.nodeName(args.currentNode))

    try Await.ready(system.whenTerminated, args.nodesCount minutes)
    finally system.terminate()

    logger.info("Done")
  }

  case class Arguments(
    nodeResolveStrategy: String = "host",
    hostname: String = "localhost",
    port: Int = 2551,
    algorithm: String = "tree",
    currentNode: Int = 0,
    nodesCount: Int = 2
  )
}
