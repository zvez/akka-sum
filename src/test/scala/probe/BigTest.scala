package probe

import java.util.concurrent.ConcurrentHashMap

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestKit
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.{FreeSpec, Matchers}
import probe.Actors.{Context, LocalNodeResolver, MasterActor, Reporter}

import scala.collection.JavaConverters._
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.Random

/**
  * Author: Andrey Zinovyev
  * Date: 05.06.2016
  */
class BigTest extends FreeSpec with Matchers with LazyLogging {

  def testAlgorithm(algName: String): Unit = {

    def runFor(nodes: Int): Unit = {
      val system = ActorSystem("akka-sum-test")
      try {
        val resultsMap = new ConcurrentHashMap[Int, Int](nodes)
        val ctx = new Context {
          val nodesCount = nodes
          val resolver = new LocalNodeResolver(system)
          val reporter = new Reporter {
            def reportResult(idx: Int, result: Int) = {
              logger.debug(s"Node $idx finished with result $result. Nodes reported: ${resultsMap.size()}")
              resultsMap.put(idx, result)
            }
          }
        }

        system.actorOf(Props.apply(new MasterActor(nodes, ctx)), Actors.masterName)

        val secretValues =
          for (idx <- 0 until nodes) yield {
            val secretValue = Random.nextInt(1000)
            system.actorOf(Actors.actorForAlg(algName, idx, ctx, secretValue), Actors.nodeName(idx))
            secretValue
          }

        val finished = TestKit.awaitCond(resultsMap.size() == nodes, nodes seconds, noThrow = true)
        val results = resultsMap.asScala.toSeq.sortBy(_._1).map(_._2).toIndexedSeq


        "all nodes should report result" in {
          finished shouldBe true
        }

        "all nodes should report same value" in {
          results.distinct.length shouldBe 1
        }

        "nodes results should be right sum value" in {
          val expectedSum = secretValues.sum
          results.head shouldBe expectedSum
        }

      } finally {
        TestKit.shutdownActorSystem(system)
      }
    }

    "tiny number of nodes (2)" - runFor(2)
    "small number of nodes (5)" - runFor(5)
    "large number of nodes (1000)" - {runFor(1000)}
  }

  Actors.algorithms.keys.foreach(alg => s"Algorithm '$alg'" - testAlgorithm(alg))
}

//object SlowTest extends Tag("SlowTest")
